package ru.t1.simanov.tm.exception.user;

public final class UserLockedException extends AbstractUserException {

    public UserLockedException() {
        super("Error! User is locked. Please contact your system administrator.");
    }

}
